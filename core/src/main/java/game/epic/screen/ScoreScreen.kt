package game.epic.screen

import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import game.epic.Epic
import game.epic.R
import game.epic.actor.CenterLabel
import game.epic.actor.Controls
import game.epic.actor.ShapeActor
import game.epic.actor.then
import game.epic.menu.Menu
import game.epic.menu.MenuScreen

/**
 * Created by jocelyn on 01.07.16.
 */
object ScoreScreen : MenuScreen() {
    val backMenu : Menu
    val labels = Array(Manager.PLAYER_NB) { CenterLabel("0", "pixel-32", Color.WHITE) }
    val bars = 0.until(Manager.PLAYER_NB).map { ShapeActor() }
    val endScores = IntArray(Manager.PLAYER_NB) { 0 }
    var min = 0
    var max = 0

    val returnInput = object : InputAdapter() {
        override fun keyUp(keycode: Int): Boolean {
            if (keycode != Input.Keys.ESCAPE) return false

            Epic.screen = FirstScreen
            return true
        }
    }

    init {
        input.addProcessor(returnInput)
        backMenu = Menu("R.E.D: RED Epic Dynamite!", listOf("Back to Main Menu" to { Epic.screen = FirstScreen }))
        push(backMenu)

        bars.forEachIndexed { i, it ->
            it.x = Controls.labels[i].x
            it.y = Controls.labels[i].y + 64f + Controls.y
            it.width = 64f
            it.height = 256f
            it.setPosition(it.x, it.y, Align.bottom)
            it.color = Color.RED
            labels[i].setPosition(it.x + 32f, it.y + it.height, Align.bottom)
            stage.addActor(labels[i])
            stage.addActor(it)
        }
    }

    override fun show() {
        stage.addActor(Controls)
        Controls.clearKeys(Color.WHITE)

        val heightIncrement = 256f / Math.abs(max - min)
        val T = 5f / Manager.ROUND_NB

        bars.forEachIndexed { i, bar ->
            var action: Action = Actions.sizeTo(bar.width, -min * heightIncrement)
            Manager.playerScores.forEach {
                val score = it[i]
                val increment = heightIncrement * if (score > 0) 4 - score else score
                action = action.then(Actions.sizeBy(0f, increment, T, Interpolation.linear))
            }

            val best = endScores.max()!!
            action = action.then {
                0.until(Manager.PLAYER_NB).forEach {
                    if (endScores[it] == best) Controls.onKey(it, Color.GOLD)
                }
            }

            bar.addAction(action)
        }

        labels.forEachIndexed { i, label ->
            var action: Action = Actions.moveTo(label.x, bars[i].y + 32f - min*heightIncrement)
            var totalScore = 0

            val randomAction = Actions.forever(Actions.delay(.01f).then {
                val rand = Manager.rand.nextInt(100) - 50
                label.setText("$rand")
            })

            Manager.playerScores.forEach {
                val score = it[i]
                val increment = if (score > 0) 4 - score else score
                totalScore += increment
                action = action.then(Actions.moveBy(0f, increment*heightIncrement, T, Interpolation.linear).then { label.setText("$totalScore") })
            }

            label.addAction(randomAction)
            action = action.then(Actions.removeAction(randomAction)).then { label.setText("$totalScore") }
            label.addAction(action)
        }
    }

}