package game.epic.screen

import game.epic.Epic
import game.epic.level.TestLevel
import game.epic.menu.Menu
import game.epic.menu.MenuScreen

object FirstScreen : MenuScreen() {
    val mainMenu: Menu
    val selectLevelMenu: Menu


    init {
        selectLevelMenu = Menu("Select Level",
                Manager.LEVELS
                        .map { it.javaClass.simpleName to { Manager.start(listOf(it)); Epic.screen = Manager } }
                        .plus( "TestLevel" to { Manager.start(listOf(TestLevel)); Epic.screen = Manager; pop() })
                        .plus("Back" to { pop() }))

        mainMenu = Menu("R.E.D: RED Epic Dynamite!",
                listOf("Start Game" to { Manager.start(); Epic.screen = Manager },
                        "Select Level" to { push(selectLevelMenu) }))
        push(mainMenu)
    }

    override fun show() {
        // while (stack.size > 1) pop()
    }
}