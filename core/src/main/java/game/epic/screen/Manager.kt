package game.epic.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import game.epic.Epic
import game.epic.actor.Controls
import game.epic.actor.then
import game.epic.level.*
import java.util.*


/**
 * Created by jocelyn on 01.07.16.
 */
object Manager : Screen() {

    const val PLAYER_NB = 4
    const val ROUND_NB = 16
    val LEVELS = listOf(TrafficLightLevel, SubThemeLevel, ColorLevel, MathLevel)

    var selectedLevels: List<Level> = emptyList()

    val playerKeys = IntArray(PLAYER_NB) { -1 }
    val playerScores = mutableListOf<IntArray>()
    val rand = Random()

    var level: Level = FirstLevel
        set(value) {
            Controls.clearKeys()
            Gdx.app.log("setlevel", value.javaClass.simpleName)
            field = value
            value.onStart()
            stage.addActor(value)
        }

    val round: Int
        get() = playerScores.size

    val progress: Float
        get() = round.toFloat() / ROUND_NB

    val playerInput = object : InputAdapter() {
        override fun keyDown(keycode: Int): Boolean {
            if (!checkIsLetter(keycode)) return false

            val playerId = playerKeys.indexOfFirst { it == keycode }
            if (level == FirstLevel || playerId != -1) {
                level.onKey(playerId, keycode)
                return true
            }
            return false
        }

        private fun checkIsLetter(keycode: Int): Boolean {
            val str = keyToChar(keycode)
            return str.length == 1 && str[0].isLetter()
        }
    }

    fun keyToChar(keycode: Int) = Input.Keys.toString(keycode)

    init {
        input.addProcessor(playerInput)
        //TODO addProcessor manage escape
    }

    fun start(selectedLevels: List<Level> = LEVELS) {
        playerKeys.fill(-1)
        playerScores.clear()
        this.selectedLevels = selectedLevels
        level = FirstLevel
        stage.addActor(Controls)
    }

    fun onEnd(points: IntArray) {
        level.remove()
        Gdx.app.log("score", Arrays.toString(points))

        playerScores.add(points)
        if (round >= ROUND_NB) {
            ScoreScreen.min = Int.MAX_VALUE
            ScoreScreen.max = Int.MIN_VALUE
            playerScores.map {
                it.zip(ScoreScreen.endScores).mapIndexed { i, pair ->
                    ScoreScreen.endScores[i] = if (pair.first > 0) {
                        (4 - pair.first) + pair.second
                    } else {
                        pair.first + pair.second
                    }
                }

                ScoreScreen.min = Math.min(ScoreScreen.min, ScoreScreen.endScores.min()!!)
                ScoreScreen.max = Math.max(ScoreScreen.max, ScoreScreen.endScores.max()!!)
            }
            Epic.screen = ScoreScreen
        } else {
            level = TransitionLevel
        }
    }

    fun nextLevel() {
        level = chooseLevel()
    }

    private fun chooseLevel(): Level {
        return selectedLevels[rand.nextInt(selectedLevels.size)]
    }
}
