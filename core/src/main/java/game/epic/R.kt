package game.epic

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.Skin

object R : AssetManager(InternalFileHandleResolver()) {
    val skin = A("ui/skin.json", Skin::class.java)
    val sprites = A("sprites.atlas", TextureAtlas::class.java)

    fun createSound(path: String) = AssetDescriptor<Sound>("sounds/$path", Sound::class.java)
    fun createMusic(path: String) = AssetDescriptor<Music>("sounds/music/$path", Music::class.java)

    val musics = listOf("jeopardy", "kombat", "oizo").map { createMusic("$it.mp3") }
    val sounds = listOf("choosechar", "shame", "pop", "pop2", "bottle", "smack", "success", "ready1", "ready2").map { createSound("$it.mp3") }

    init {
        loadNow(skin)
        musics.forEach { loadNow(it) }
        sounds.forEach { loadNow(it) }
        loadAll()
        finishLoading()
    }

    fun loadNow(asset: AssetDescriptor<*>) {
        load(asset)
        finishLoadingAsset(asset.fileName)
    }

    fun loadAll() = R::class.java.declaredMethods
            .filter { it.name.startsWith("get") and it.returnType.isAssignableFrom(AssetDescriptor::class.java) }
            .forEach { load(it(R) as AssetDescriptor<*>?) }

    private fun <T> A(fileName: String, type: Class<T>) = AssetDescriptor(fileName, type)
}