package game.epic.level

import com.badlogic.gdx.scenes.scene2d.Group
import game.epic.screen.Manager

/**
 * Created by jocelyn on 01.07.16.
 */
abstract class Level : Group() {

    val points = IntArray(Manager.PLAYER_NB, { 0 })

    abstract fun onKey(playerId: Int, keycode: Int)

    abstract fun onStart()

    open fun onEnd() {
    }

    open fun nextLevel() {
        onEnd()
        Manager.onEnd(points.copyOf())
        points.fill(0)
    }

}