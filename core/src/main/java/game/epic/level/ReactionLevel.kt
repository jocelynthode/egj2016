package game.epic.level

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import game.epic.R
import game.epic.actor.Controls
import game.epic.audio.Sounds
import game.epic.screen.Manager

/**
 * Created by jocelyn on 02.07.16.
 */
abstract class ReactionLevel : Level() {
    val MIN_DELAY = 0.4f to 1f
    val MAX_DELAY = 1f to 1.6f

    var currentResult: Int? = null
    var targetResult: Int? = null
    var nbCycles = 0

    abstract fun maxNbCycles(): Int
    abstract fun randomResult(value: Int = -1): Int
    fun randomDelay(): Float {
        val progress = Manager.round.toFloat() / Manager.ROUND_NB
        val min = MIN_DELAY.second - (MIN_DELAY.second - MIN_DELAY.first) * progress
        val max = MAX_DELAY.second - (MAX_DELAY.second - MAX_DELAY.first) * progress
        return min + (max - min) * Manager.rand.nextFloat()
    }

    fun chooseResult() {
        // TODO make this check after new color is chosen and choose non-random delay
        if (currentResult == targetResult) {
            nextLevel()
            return
        }

        nbCycles++
        if (nbCycles >= maxNbCycles()) {
            currentResult = randomResult(targetResult!!)
        } else {
            val lastResult = currentResult
            while (lastResult == currentResult) {
                currentResult = randomResult()
            }
        }
        val delay = randomDelay()
        val action = Actions.delay(delay, Actions.run { chooseResult() })
        addAction(action)
    }

    override fun onKey(playerId: Int, keycode: Int) {
        if (points[playerId] != 0) return

        if (currentResult == targetResult) {
            points[playerId] = points.max()!! + 1
            Controls.onKey(playerId, Color.GREEN)
            Sounds.SUCCESS.play()
        } else {
            points[playerId] = points.min()!! - 1
            Controls.onKey(playerId, Color.RED)
            Sounds.SHAME.play()
        }
    }

    override fun onStart() {
        currentResult = null
        targetResult = randomResult()
        nbCycles = 0
        Controls.clearKeys(Color.WHITE)
    }
}