package game.epic.level

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.utils.Align
import game.epic.Epic
import game.epic.actor.CenterLabel
import game.epic.actor.Controls
import game.epic.actor.then
import game.epic.audio.Sounds
import game.epic.screen.Manager

/**
 * Created by jocelyn on 02.07.16.
 */
abstract class SpamLevel() : Level() {
    var progress = IntArray(Manager.PLAYER_NB) { 0 }
    var maxProgress = 25
    var elapsedTime = 0f
    var maxElapsedTime = 7f
    val timeLabel = CenterLabel("", "pixel-64",Color.WHITE)

    val labels = progress.map { CenterLabel("$it", "pixel-32", Color.WHITE) }

    init {
        timeLabel.setPosition(0f, Epic.HEIGHT/2 - 100f)
        labels.forEachIndexed { i, it ->
            it.setPosition(-180f + 130 * i, -250f, Align.bottom)
            addActor(it)
        }
    }

    abstract fun spam(playerId: Int)

    override fun onKey(playerId: Int, keycode: Int) {
        if (points[playerId] != 0) return

        Controls.onKey(playerId, Color.WHITE)
        val action = Actions.delay(0.05f).then { Controls.onKey(playerId, Color.GRAY) }
        addAction(action)

        progress[playerId]++
        spam(playerId)
        if (progress[playerId] >= maxProgress) {
            points[playerId] = points.max()!! + 1
            clearKeys()
            clearActions()
            Controls.onKey(playerId, Color.GREEN)
            if (!progress.contains(0)) nextLevel()
        }
    }

    fun clearKeys() {
        progress.forEachIndexed { i, p ->
            if (p >= maxProgress)
                Controls.onKey(i, Color.GREEN)
            else
                Controls.onKey(i, Color.GRAY)
        }
    }

    override fun onStart() {
        progress.fill(0)
        elapsedTime = 0f
        maxElapsedTime = 7f - (4 * Manager.progress)
        clearChildren()
        Controls.clearKeys(Color.GRAY)
        timeLabel.setText("%.1f".format(maxElapsedTime))
        addActor(timeLabel)
        Sounds.KOMBAT.volume = 0.25f
        Sounds.KOMBAT.play()
    }

    override fun onEnd() = Sounds.KOMBAT.stop()

    override fun act(delta: Float) {
        super.act(delta)
        timeLabel.remove()
        elapsedTime += delta
        // TODO move this to Action; don't forget to clearActions()
        if (elapsedTime >= maxElapsedTime) {
            timeLabel.setText("0.0")
            Gdx.app.log("SpamLevel", "Time's over!")
            progress
                    .mapIndexed { i, p -> i to p }
                    .filter { it.second < maxProgress }
                    .sortedBy { -it.second }
                    .forEachIndexed { i, pair -> points[pair.first] = -(i + 1) }

            nextLevel()
        } else {
            timeLabel.setText("%.1f".format(maxElapsedTime - elapsedTime))
        }

        addActor(timeLabel)
        labels.forEachIndexed { i, it -> it.remove(); addActor(it); it.setText("${progress[i]}") }
    }
}