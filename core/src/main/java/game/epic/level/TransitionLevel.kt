package game.epic.level

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import game.epic.actor.CenterLabel
import game.epic.actor.then
import game.epic.audio.Sounds
import game.epic.screen.Manager
import io.spacepiano.actors.Transformer

/**
 * Created by isma on 02.07.16.
 */
object TransitionLevel : Level() {
    val labels = listOf("arial", "comfortaa", "garamond", "gentium", "lobster", "pixel", "theokritos")
            .map { "$it-64" }
            .map { CenterLabel("Get Ready!", it, Color.WHITE) }
            .map { it to Transformer(it) }

    override fun onKey(playerId: Int, keycode: Int) {
    }

    override fun onStart() {
        val (label, actor) = Manager.rand.nextInt(labels.size).let { labels[it] }
        // label.setText("")
        val action = Actions.rotateBy(360f, 1f, Interpolation.elasticOut)
                .then { Actions.delay(1f) }
                .then { nextLevel(); actor.remove() }
        actor.addAction(action)
        addActor(actor)
        Sounds.ready[Manager.rand.nextInt(Sounds.ready.size)].play()
    }

    override fun nextLevel() {
        remove()
        Manager.nextLevel()
    }
}