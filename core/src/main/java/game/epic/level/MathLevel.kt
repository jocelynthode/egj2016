package game.epic.level

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import game.epic.R
import game.epic.actor.CenterLabel
import game.epic.actor.Controls
import game.epic.audio.Sounds
import game.epic.screen.Manager

/**
 * Created by jocelyn on 02.07.16.
 */
object MathLevel : ReactionLevel() {

    const val MAX_NB = 9

    override fun maxNbCycles() = MAX_NB

    override fun randomResult(value: Int): Int {
        val result = if (value == -1) Manager.rand.nextInt(MAX_NB) + 1 else value
        val progress = Manager.round.toFloat() / Manager.ROUND_NB
        val exprDifficulty = (progress * 4).toInt()
        val exprType = Manager.rand.nextInt(exprDifficulty + 1)
        val exprText = when (exprType) {
            0 -> {
                // Addition
                val firstInt = Manager.rand.nextInt(result + 1)
                "$firstInt + ${result - firstInt}"
            }
            1 -> {
                // Subtraction
                val a = Manager.rand.nextInt(result) + result
                "$a - ${a - result}"
            }
            2 -> {
                // Multiplication
                val (a, b) = 1.until(result + 1)
                        .filter { result % it == 0 }
                        .map { it to result / it }
                        .let { it[Manager.rand.nextInt(it.size)] }
                "$a x $b"
            }
            3 -> {
                // Division
                val divisor = Manager.rand.nextInt(result) + 1
                "${result * divisor} / $divisor"
            }
            else -> "?"
        }
        MathActor.label.setText(exprText)
        return result
    }

    override fun onStart() {
        super.onStart()
        MathActor.remove()
        addActor(TargetActor)
        TargetActor.label.setText("? = ${targetResult}")

        val action = Actions.delay(3f, Actions.run {
            TargetActor.remove()
            chooseResult()
            addActor(MathActor)
            Sounds.JEOPARDY.play()
        })
        addAction(action)
    }

    override fun onEnd() = Sounds.JEOPARDY.stop()

    object MathActor : Group() {
        val label = CenterLabel("", "gentium-128", Color.WHITE)

        init {
            addActor(label)
        }
    }

    object TargetActor : Group() {
        val label = CenterLabel("", "gentium-128", Color.WHITE)

        init {
            addActor(label)
            label.setFontScale(2f)
        }
    }
}