package game.epic.level

import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import game.epic.Epic
import game.epic.actor.CenterLabel
import game.epic.audio.Sounds
import game.epic.screen.Manager

/**
 * Created by jocelyn on 01.07.16.
 */
object ColorLevel : ReactionLevel() {

    val COLORS = listOf("RED" to Color.RED, "GREEN" to Color.GREEN, "BLUE" to Color.BLUE, "YELLOW" to Color.YELLOW,
            "PURPLE" to Color.PURPLE)

    override fun maxNbCycles() = COLORS.size

    override fun randomResult(value: Int) = if (value == -1) Manager.rand.nextInt(COLORS.size) else value

    override fun onStart() {
        super.onStart()
        ColorActor.zIndex = 0

        ColorActor.remove()
        addActor(TargetActor)
        TargetActor.label.setText(COLORS[targetResult!!].first)

        val action = Actions.delay(3f, Actions.run {
            TargetActor.remove()
            chooseResult()
            addActor(ColorActor)
            Sounds.OIZO.volume = 0.7f
            Sounds.OIZO.play()
        })
        addAction(action)
    }

    override fun onEnd() = Sounds.OIZO.stop()

    object ColorActor : Actor() {
        val texture: Texture

        init {
            val pixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
            pixmap.setColor(Color.WHITE)
            pixmap.fillRectangle(0, 0, 1, 1)
            texture = Texture(pixmap)
            pixmap.dispose()
        }

        override fun draw(batch: Batch?, parentAlpha: Float) {
            batch?.color =
                    if (currentResult == null) Color.BLACK
                    else COLORS[currentResult!!].second

            batch?.draw(texture, -Epic.WIDTH/2, -Epic.HEIGHT/2 + 140, Epic.WIDTH, Epic.HEIGHT-128)
        }
    }

    object TargetActor : Group() {
        val label = CenterLabel("", "lobster-128", Color.WHITE)

        init {
            addActor(label)
        }
    }
}