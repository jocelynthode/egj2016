package game.epic.level

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.utils.Align
import game.epic.Epic
import game.epic.R
import game.epic.audio.Sounds
import game.epic.screen.Manager

/**
 * Created by jocelyn on 02.07.16.
 */
object SubThemeLevel : SpamLevel() {

    val sprites = listOf("cheese", "courgette", "unicorn", "heart").map {
        R[R.sprites].createSprite("half_$it")
    }

    override fun onStart() {
        super.onStart()
        maxProgress = 20
    }

    override fun spam(playerId: Int) {
        val image = Image(sprites[playerId])
        val x = (Manager.rand.nextFloat() - 0.5f) * Epic.WIDTH
        val y = (Manager.rand.nextFloat() - 0.5f) * (Epic.HEIGHT-140) + 140
        image.setPosition(x, y, Align.center)
        image.zIndex = 1001
        addActor(image)

        val T = .2f
        val action = Actions.forever(
                Actions.parallel(
                    Actions.sequence(
                            Actions.scaleTo(1.5f, 1.5f, T, Interpolation.elastic),
                            Actions.scaleTo(1.0f, 1.0f, T, Interpolation.elastic)
                    ),
                    Actions.sequence(
                            Actions.rotateBy(+10f, T, Interpolation.elastic),
                            Actions.rotateBy(-10f, T, Interpolation.elastic)
                    )
                )
        )

        image.addAction(action)
        when (playerId) {
            0 -> Sounds.POP.play()
            1 -> Sounds.BOTTLE.play()
            2 -> Sounds.POP2.play()
            3 -> Sounds.SMACK.play()
        }
    }
}