package game.epic.level

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import game.epic.R
import game.epic.actor.Controls
import game.epic.audio.Sounds
import game.epic.screen.Manager

/**
 * Created by jocelyn on 01.07.16.
 */
object FirstLevel : Level() {
    val title = Label("CHOOSE\nYOUR\nCHARACTER", R[R.skin], "keys-32", Color.WHITE)

    init {
        title.setAlignment(Align.center)
        title.setPosition(0f, 200f, Align.top)
        addActor(title)
    }

    override fun onKey(playerId: Int, keycode: Int) {
        if (playerId != -1) return
        val i = Manager.playerKeys.indexOfFirst { it == -1 }
        if (i != -1) Manager.playerKeys[i] = keycode
        else return

        Gdx.app.log("FirstLevel", "Player $playerId = [${Manager.keyToChar(keycode)}]")

        Controls.onKey(i, Color.WHITE)

        when (i) {
            0 -> Sounds.POP.play()
            1 -> Sounds.BOTTLE.play()
            2 -> Sounds.POP2.play()
            3 -> Sounds.SMACK.play()
        }

        if (i == Manager.PLAYER_NB - 1) {
            val action = Actions.delay(2f, Actions.run { nextLevel() })
            addAction(action)
        }
    }

    override fun onStart() {
        Sounds.CHOOSE_CHARACTER.play()
    }
}