package game.epic.level

import com.badlogic.gdx.Gdx
import game.epic.screen.Manager

/**
 * Created by isma on 02.07.16.
 */
object TestLevel : Level() {
    override fun onKey(playerId: Int, keycode: Int) {
        Gdx.app.log("TestLevel", "onKey")
        points.indices.forEach {
            points[it] = Manager.rand.nextInt(9) - 4
        }
        nextLevel()
    }

    override fun onStart() {
        Gdx.app.log("TestLevel", "onStart")
    }
}