package game.epic.level

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.utils.Align
import game.epic.Epic
import game.epic.R
import game.epic.actor.CenterLabel
import game.epic.actor.Controls
import game.epic.actor.then
import game.epic.audio.Sounds
import game.epic.screen.Manager

/**
 * Created by jocelyn on 02.07.16.
 */
object TrafficLightLevel : SpamLevel() {
    var currentState = 2
    val images = (0..3).map { R[R.sprites].createSprite("light-$it").let { Image(it) } }

    init {
        images.forEach {
            it.setScale(.5f)
            it.setOrigin(Align.center)
            it.setPosition(0f, 0f, Align.center)
        }
    }

    override fun onStart() {
        maxProgress = 15
        super.onStart()
        setState(2)
        labels.forEach { addActor(it) }
    }

    fun setState(state: Int) {
        if (state == 3)
            Sounds.KOMBAT.volume = 0.25f
        else
            Sounds.KOMBAT.volume = 0f
        images[currentState].remove()
        currentState = state
        addActor(images[state])
        val nextState = Manager.rand.nextInt(2) * 2 + 1
        val delay = if (state == 2) 2f else 1f + Manager.rand.nextFloat()
        val action = Actions.delay(delay).then { setState(nextState) }
        addAction(action)
    }

    override fun spam(playerId: Int) {
        if (currentState != 3) {
            Sounds.SHAME.play()
            progress[playerId] -= 2
            Controls.onKey(playerId, Color.RED)
        } else {
            when (playerId) {
                0 -> Sounds.POP.play()
                1 -> Sounds.BOTTLE.play()
                2 -> Sounds.POP2.play()
                3 -> Sounds.SMACK.play()
            }
        }
    }

    override fun act(delta: Float) {
        super.act(delta)
        if (currentState != 3) elapsedTime -= delta
    }
}