package game.epic.audio

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import game.epic.R

/**
 * Created by simon on 03.07.16.
 */

object Sounds {

        val CHOOSE_CHARACTER = R[R.sounds[0]]
        val SHAME = R[R.sounds[1]]
        val POP = R[R.sounds[2]]
        val POP2 = R[R.sounds[3]]
        val BOTTLE = R[R.sounds[4]]
        val SMACK = R[R.sounds[5]]
        val SUCCESS =  R[R.sounds[6]]
        val ready = listOf(R[R.sounds[7]], R[R.sounds[8]])

        val JEOPARDY =  R[R.musics[0]]
        val KOMBAT = R[R.musics[1]]
        val OIZO =  R[R.musics[2]]
}