package game.epic

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import game.epic.screen.FirstScreen
import game.epic.screen.Screen

/** [com.badlogic.gdx.ApplicationListener] implementation shared by all platforms.  */
object Epic : Game() {
    val WIDTH = 1024f
    val HEIGHT = 768f

    override fun create() {
        Gdx.app.logLevel = 0
        setScreen(FirstScreen)
    }

    override fun setScreen(screen: com.badlogic.gdx.Screen?) {
        super.setScreen(screen)
        Gdx.app.log("setScreen", screen?.javaClass?.simpleName)
        Gdx.input.inputProcessor = when (screen) {
            is Screen -> screen.input
            else -> null
        }
    }

    override fun render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        super.render()
    }

    override fun dispose() {
    }
}