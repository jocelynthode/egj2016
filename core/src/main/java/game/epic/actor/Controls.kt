package game.epic.actor

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import game.epic.Epic
import game.epic.R
import game.epic.screen.Manager

/**
 * Created by jocelyn on 03.07.16.
 */
object Controls : Group() {
    val labels = 0.until(Manager.PLAYER_NB).map { Label("", R[R.skin], "keys-128", Color.WHITE) }

    init {
        setPosition(0f, -Epic.HEIGHT/2 + 100)
        Controls.zIndex = 1000
        labels.forEachIndexed { i, label ->
            label.setAlignment(Align.center)
            label.setPosition(-180f + 130 * i, 0f, Align.bottom)
            addActor(label)
        }
    }

    fun onKey(playerId: Int, color: Color) {
        val label = labels[playerId]
        label.color = color
        label.setText(Manager.keyToChar(Manager.playerKeys[playerId]))
    }

    fun clearKeys(color: Color = Color.CLEAR) {
        labels.forEach { it.color = color }
    }
}