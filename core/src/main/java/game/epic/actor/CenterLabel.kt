package game.epic.actor

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Align
import game.epic.R

/**
 * Created by isma on 02.07.16.
 */
class CenterLabel(text: String, font: String, color: Color) : Label(text, R[R.skin], font, color) {
    init {
        setAlignment(Align.center)
        setPosition(0f, 0f)
    }

    override fun setPosition(x: Float, y: Float) {
        super.setPosition(x, y, Align.center)
    }
}