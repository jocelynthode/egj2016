package game.epic.actor

import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.actions.Actions

/**
 * Created by isma on 02.07.16.
 */

fun Action.then(action: Action): Action = Actions.sequence(this, action)

fun Action.then(action: () -> Unit): Action = this.then(Actions.run(action))

