package game.epic.actor

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor

/**
 * Created by isma on 03.07.16.
 */

class ShapeActor : Actor() {
    val texture: Texture

    init {
        val pixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
        pixmap.setColor(Color.WHITE)
        pixmap.fillRectangle(0, 0, 1, 1)
        texture = Texture(pixmap)
        pixmap.dispose()
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        batch?.color = color
        batch?.draw(texture, x, y, width, height)
    }
}
